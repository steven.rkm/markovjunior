const tilesRiver = new TileSet('River', [
    new Tile('NOTHING', 0, 0, 0),
    new Tile('RED', 0, 100, 80),
    new Tile('WHITE', 0, 0, 100),
    new Tile('WATER', 50, 70, 70),
    new Tile('GRASS', 30, 70, 70),
    new Tile('FOREST', 30, 70, 40),
])

const tilesBacktrackMaze = new TileSet('Backtrack Maze', [
    new Tile('NOTHING', 0, 0, 0),
    new Tile('CURSOR', 0, 100, 80),
    new Tile('PATH', 30, 70, 70),
    new Tile('BACKTRACK', 0, 0, 100),
])

const tilesSokoban = new TileSet('Sokoban', [
    new Tile('NOTHING', 0, 0, 0),
    new Tile('AGENT', 0, 100, 80),
    new Tile('BOX', 0, 0, 100),
])

const tilesGeneral = new TileSet('General', [
    new Tile('BLACK', 0, 0, 0),
    new Tile('WHITE', 0, 0, 100),
    new Tile('RED', 0, 100, 80),
    new Tile('GREEN', 30, 70, 70),
    new Tile('BLUE', 50, 70, 70),
])
