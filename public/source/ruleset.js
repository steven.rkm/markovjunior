class RuleSet {
    constructor({ name, tileSetName, backgroundTileName, rules, settings, fixedTiles }) {
        if (!rules || rules.length === 0) {
            throw new Error('RuleSet: rules is empty')
        }
        if (!name || name.trim() === '') {
            throw new Error('RuleSet: name is empty')
        }
        if (!tileSetName || tileSetName.trim() === '') {
            throw new Error('RuleSet: tileSetName is empty')
        }
        if (!backgroundTileName || backgroundTileName.trim() === '') {
            throw new Error('RuleSet: backgroundTileName is empty')
        }

        this.name = name.trim()
        this.tileSetName = tileSetName.trim()
        this.backgroundTileName = backgroundTileName.trim()
        this.rules = rules
        this.settings = settings ?? {}
        this.fixedTiles = fixedTiles ?? []
    }

    init(gridWidth, gridHeight, tileset) {
        this.rules.forEach((rule) => rule.init(gridWidth, gridHeight, tileset))
    }

    // SERIALIZATION
    static ruleFromJSON(ruleObj) {
        if (ruleObj.type === 'SplitRule') {
            return new SplitRule(
                ruleObj.rules.map((r) => RuleSet.ruleFromJSON(r)),
                {
                    limit: ruleObj.limit,
                    description: ruleObj.description,
                },
            )
        } else if (ruleObj.type === 'ReplaceAllRule') {
            return new ReplaceAllRule(ruleObj.inputTiles, ruleObj.outputTiles, {
                limit: ruleObj.limit,
                description: ruleObj.description,
            })
        } else if (ruleObj.type === 'ReplaceRule') {
            return new ReplaceRule(ruleObj.inputTiles, ruleObj.outputTiles, {
                limit: ruleObj.limit,
                description: ruleObj.description,
            })
        }
    }

    static fromJSON(json) {
        const obj = typeof json === 'string' ? JSON.parse(json) : json

        console.log(obj)

        return new RuleSet({
            ...obj,
            rules: obj.rules.map((rule) => RuleSet.ruleFromJSON(rule)),
        })
    }

    static ruleToJSON(rule) {
        if (rule instanceof SplitRule) {
            return {
                type: 'SplitRule',
                rules: rule.rules.map((r) => RuleSet.ruleToJSON(r)),
                description: rule.description,
            }
        } else if (rule instanceof ReplaceAllRule) {
            return {
                type: 'ReplaceAllRule',
                inputTiles: rule.inputTiles,
                outputTiles: rule.outputTiles,
                limit: rule.limit,
                description: rule.description,
            }
        } else if (rule instanceof ReplaceRule) {
            return {
                type: 'ReplaceRule',
                inputTiles: rule.inputTiles,
                outputTiles: rule.outputTiles,
                limit: rule.limit,
                description: rule.description,
            }
        }
    }

    toJSON() {
        const jsonObj = {
            name: this.name,
            tileSetName: this.tileSetName,
            backgroundTileName: this.backgroundTileName,
            rules: this.rules.map((r) => RuleSet.ruleToJSON(r)),
            settings: this.settings,
            fixedTiles: this.fixedTiles,
        }

        return JSON.stringify(jsonObj, null, 2)
    }

    saveToLocalStorage(key) {
        const json = this.toJSON()
        localStorage.setItem(key, json)
    }

    static loadFromLocalStorage(key) {
        const json = localStorage.getItem(key)
        if (json) {
            return RuleSet.fromJSON(json)
        }
        return null
    }
}

// Export to JSON string
// const jsonString = RuleSet.toJSONString(ruleSets);
// console.log(jsonString);
//
// // Import from JSON string
// const loadedRuleSets = RuleSet.fromJSONString(jsonString);
// console.log(loadedRuleSets);
//
// // Save to localStorage
// RuleSet.saveToLocalStorage(ruleSets, 'myRuleSets');
//
// // Load from localStorage
// const loadedFromLocalStorage = RuleSet.loadFromLocalStorage('myRuleSets');
// console.log(loadedFromLocalStorage);
