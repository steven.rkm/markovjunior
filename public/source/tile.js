// Represents a value to color

class Tile {
    constructor(name, h, s, l) {
        this.name = name
        this.h = h
        this.s = s
        this.l = l
        this.value = undefined
    }
}
