class TileSet {
    constructor(name, tiles) {
        this.name = name
        this.tiles = tiles
        this.tiles_by_name = {}
        this.tiles_by_value = {}

        for (let i = 0; i < tiles.length; i++) {
            const tile = tiles[i]
            tile.value = i // value used in grid!
            this.tiles_by_name[tile.name] = tile
            this.tiles_by_value[tile.value] = tile
            this.tiles_by_value[tile.value] = tile
        }
    }

    getByName(name) {
        const tile = this.tiles_by_name[name]
        if (tile === undefined) {
            throw new Error(`Could not find color by name: ${name}`)
        }
        return tile
    }
    getByValue(value) {
        const tile = this.tiles_by_value[value]
        if (tile === undefined) {
            throw new Error(`Could not find color by value: ${value}`)
        }
        return tile
    }
}
