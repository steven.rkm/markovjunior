// Lifecycle:
// init -- called once when ruleset is loaded or reset
//
/*

init -- called once when ruleset is loaded or reset

isAlive() => boolean                    -> if rule is dead, can't be applied or updated anymore
isApplicable (Grid) => boolean          -> if this rule can be applied or not
apply (Grid) => [rule, coord{x,y}[]]    -> apply to a random option, and return
                                                                itself if it was applied
                                                                and the list of pixels that were changed
updateOptions (Grid, coord{x,y}[]) =>   -> adjust the cached options with the updated pixels

beginning:
for rule in ruleset:
    if rule is applicable
    then applu and go to beginning
    else go to next rule

simulation stops if no rule was applied
*/

const defaultOptions = {
    limit: undefined,
    description: undefined,
}

class Rule {
    constructor(options = {}) {
        const { limit, description } = {
            ...defaultOptions,
            ...options,
        }

        if (new.target === Rule) {
            throw new TypeError('Cannot construct Rule instances directly')
        }

        this.limit = limit
        this.description = description

        // used during runtime

        this.limitCount = 0
        this.isAlive = true
    }

    init() {
        this.limitCount = 0
        this.isAlive = true

        if (this.description) console.log(this.description)
    }

    isActive(activeRule) {
        throw new Error("Method 'isActive()' must be implemented.")
    }

    isApplicable(grid) {
        throw new Error("Method 'isApplicable()' must be implemented.")
    }

    apply(grid) {
        if (this.limit !== undefined) {
            this.limitCount++
            this.isAlive = this.limitCount < this.limit
        }
    }

    updateOptions(grid, coords) {
        throw new Error("Method 'updateOptions()' must be implemented.")
    }

    // TODO drawing should be outsourced to a different class, and canvas should be passed
    draw(activeRule) {
        throw new Error("Method 'draw()' must be implemented.")
    }
}
