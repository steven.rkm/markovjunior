const UP = 0
const DOWN = 1
const LEFT = 2
const RIGHT = 3

const directions = [UP, DOWN, LEFT, RIGHT]

const DIRECTION_MAPPING = {
    [UP]: [0, 1],
    [DOWN]: [0, -1],
    [LEFT]: [-1, 0],
    [RIGHT]: [1, 0],
}

const EMPTY = -1

const TILESET_REGISTRY = new Registry()
const RULESET_REGISTRY = new Registry()
