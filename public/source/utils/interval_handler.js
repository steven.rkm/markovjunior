class IntervalHandler {
    constructor(callback, interval) {
        this.intervalId = null
        this.callback = callback
        this.interval = interval
    }

    start() {
        if (this.callback !== null && this.interval !== null) {
            this.pause()
            this.intervalId = setInterval(this.callback, this.interval)
        }
    }

    pause() {
        if (this.intervalId !== null) {
            clearInterval(this.intervalId)
            this.intervalId = null
        }
    }

    isPaused() {
        return this.intervalId === null
    }
}
