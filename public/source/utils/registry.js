class Registry {
    constructor() {
        this.registry = {}
    }

    register(obj) {
        if (obj.name === undefined) {
            throw new Error(`Object must have name property.`)
        }
        if (this.registry[obj.name]) {
            throw new Error(`An object with the name '${obj.name}' is already registered.`)
        }
        this.registry[obj.name] = obj
    }

    get(name) {
        const obj = this.registry[name]
        if (obj === undefined) {
            throw new Error(`Could not find: ${name}`)
        }
        return obj
    }

    list() {
        return Object.keys(this.registry)
    }
}
