class Grid {
    constructor(rows, cols, initialValue = null) {
        this.rows = rows
        this.cols = cols
        this.grid = Array.from({ length: rows }, () => Array(cols).fill(initialValue))
    }

    // Get value at a specific cell
    get(row, col) {
        // if (row < 0 || row >= this.rows || col < 0 || col >= this.cols) {
        //     throw new Error("Index out of bounds");
        // }
        return this.grid[row][col]
    }

    // Set value at a specific cell
    set(row, col, value) {
        // if (row < 0 || row >= this.rows || col < 0 || col >= this.cols) {
        //     throw new Error("Index out of bounds");
        // }
        this.grid[row][col] = value
    }

    // Fill the entire grid with a specific value
    fill(value) {
        for (let row = 0; row < this.rows; row++) {
            this.grid[row].fill(value)
        }
    }

    // Create an edge around the grid with a specific value
    makeEdge(edgeValue) {
        // Top and bottom rows
        for (let col = 0; col < this.cols; col++) {
            this.set(0, col, edgeValue)
            this.set(this.rows - 1, col, edgeValue)
        }

        // Left and right columns
        for (let row = 0; row < this.rows; row++) {
            this.set(row, 0, edgeValue)
            this.set(row, this.cols - 1, edgeValue)
        }
    }

    // Print the grid to the console
    print() {
        for (let row = 0; row < this.rows; row++) {
            console.log(this.grid[row].join(' '))
        }
    }
}
