/* Basic rule that replaces a line of tiles with another line */

class ReplaceRule extends Rule {
    constructor(inputTiles, outputTiles, options) {
        super(options)

        this.inputTiles = inputTiles
        this.outputTiles = outputTiles

        // no need to check all directions when the rule has only once tile
        this.directions = inputTiles.length === 1 ? [UP] : directions

        // used during runtime

        this.inputTileValues = undefined
        this.outputTileValues = undefined

        this.optionsGrid = undefined
        this.isOptionsGridReady = false // data in optionsArray is ready to be used or not
        this.optionsArray = []
    }

    init(gridWidth, gridHeight, tileset) {
        super.init()

        // setup mapping from tile name to value
        this.inputTileValues = this.inputTiles.map((name) => tileset.getByName(name).value)
        this.outputTileValues = this.outputTiles.map((name) => tileset.getByName(name).value)

        // if gridsize stays the same, no need to recreate it
        if (!this.optionsGrid || this.optionsGrid.rows !== gridWidth || this.optionsGrid.cols !== gridHeight) {
            this.optionsGrid = new Grid(gridWidth, gridHeight, undefined)
            // reset values
            for (let i = 0; i < gridWidth; i++) {
                for (let j = 0; j < gridHeight; j++) {
                    this.optionsGrid.set(
                        i,
                        j,
                        this.inputTileValues.length === 1 ? [false] : [false, false, false, false],
                    )
                }
            }
        }

        this.isOptionsGridReady = false
        this.optionsArray = []
    }

    isActive(activeRule) {
        return this === activeRule
    }

    isApplicable(grid) {
        if (!this.isOptionsGridReady) {
            this.prepareOptionsGrid(grid)
            this.isOptionsGridReady = true
        }

        return this.optionsArray.length > 0
    }

    apply(grid) {
        super.apply()

        const [i, j, direction] = getRandomValueFromArray(this.optionsArray)

        const coords = this.applyToOption(grid, i, j, direction)
        return [this, coords]
    }

    prepareOptionsGrid(grid) {
        // returns a list of all options for that rule to match

        for (let i = 0; i < this.optionsGrid.rows; i++) {
            for (let j = 0; j < this.optionsGrid.cols; j++) {
                for (let k = 0; k < this.directions.length; k++) {
                    const applicable = this.isValidForOption(grid, i, j, this.directions[k])
                    this.optionsGrid.grid[i][j][k] = applicable
                    if (applicable) {
                        this.optionsArray.push([i, j, this.directions[k]])
                    }
                }
            }
        }
    }

    updateOptions(grid, coords) {
        if (!this.isOptionsGridReady) {
            // console.warn('isOptionsGridReady should be true!')
            return
        }

        for (const [i, j] of coords) {
            for (let k = 0; k < this.directions.length; k++) {
                const wasApplicable = this.optionsGrid.grid[i][j][k]
                const applicable = this.isValidForOption(grid, i, j, this.directions[k])

                this.optionsGrid.grid[i][j][k] = applicable

                // update the count
                if (wasApplicable !== applicable) {
                    if (applicable) {
                        this.optionsArray.push([i, j, this.directions[k]])
                    } else {
                        for (let l = 0; l < this.optionsArray.length; l++) {
                            const option = this.optionsArray[l]
                            if (option[0] === i && option[1] === j && option[2] === this.directions[k]) {
                                this.optionsArray.splice(l, 1)
                                break // Exit the loop after removing the first matching object
                            }
                        }
                    }
                }
            }
        }

        // console.log('-- update coords', coords, this.optionsArray, this.optionsGrid)
    }

    isValidForOption(grid, x, y, direction) {
        const [dX, dY] = DIRECTION_MAPPING[direction]

        let currentX = x
        let currentY = y
        const { inputTileValues } = this
        for (let i = 0; i < inputTileValues.length; i++) {
            if (getCell(grid, currentX, currentY) !== inputTileValues[i]) {
                return false
            }
            // check next cell
            currentX += dX
            currentY += dY
        }

        return true
    }

    applyToOption(grid, x, y, direction) {
        // do the actual replace
        const [dX, dY] = DIRECTION_MAPPING[direction]

        const updatedCoords = []

        let currentX = x
        let currentY = y
        const { inputTileValues, outputTileValues } = this
        for (let i = 0; i < inputTileValues.length; i++) {
            grid.set(currentX, currentY, outputTileValues[i])

            updateCell(currentX, currentY, outputTileValues[i])

            updatedCoords.push([currentX, currentY])

            // move to next cell
            currentX += dX
            currentY += dY
        }

        return updatedCoords
    }

    draw(activeRule) {
        const cellSizeForRules = 20

        push()

        const { inputTileValues, outputTileValues } = this

        // highlight
        if (this.isActive(activeRule)) {
            fill(0, 0, 50)
            const length = inputTileValues.length + outputTileValues.length
            const margin = 5
            rect(-margin, -margin, cellSizeForRules * (1 + length) + margin * 2, cellSizeForRules + margin * 2)
        }

        for (const cell of inputTileValues) {
            drawTile(cell, cellSizeForRules)
            translate(cellSizeForRules, 0)
        }

        fill(0, 100, 80)
        circle(cellSizeForRules / 2, cellSizeForRules / 2, cellSizeForRules / 2)
        translate(cellSizeForRules, 0)

        for (const cell of outputTileValues) {
            drawTile(cell, cellSizeForRules)
            translate(cellSizeForRules, 0)
        }

        if (this.limit) {
            noStroke()
            textSize(cellSizeForRules)
            const limitReached = this.limit === this.limitCount
            if (limitReached) {
                fill(0, 100, 80)
            } else {
                fill(0, 0, 100)
            }
            text(`${this.limitCount} / ${this.limit}`, 10, cellSizeForRules - 3)
        }

        pop()

        translate(0, cellSizeForRules * 1.5)
    }
}
