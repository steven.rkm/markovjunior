/* Picks a random applicable rule to apply */

class SplitRule extends Rule {
    constructor(rules, options) {
        super({
            description: 'Split between:',
            ...options,
        })
        this.rules = rules

        this.applicableRules = []
    }

    init(gridWidth, gridHeight, tileset) {
        super.init()
        // TODO pass parents?
        this.rules.forEach((rule) => rule.init(gridWidth, gridHeight, tileset))
    }

    isActive(activeRule) {
        const atLeastOneActive = this.rules.find((rule) => rule.isActive(activeRule))
        return atLeastOneActive !== undefined
    }

    isApplicable(grid) {
        this.applicableRules = this.rules.filter((rule) => rule.isAlive && rule.isApplicable(grid))

        return this.applicableRules.length > 0
    }

    apply(grid) {
        super.apply()

        const randomRule = getRandomValueFromArray(this.applicableRules)

        const [rule, coords] = randomRule.apply(grid)

        return [rule, coords]
    }

    updateOptions(grid, coords) {
        this.rules.forEach((rule) => rule.updateOptions(grid, coords))
    }

    draw(activeRule) {
        const cellSizeForRules = 20

        // console.log(drawingContext.getTransform())

        const { from, to } = this

        // highlight
        if (this.isActive(activeRule)) {
            fill(0, 0, 50)
            const margin = 5
            rect(-margin, -margin, cellSizeForRules, cellSizeForRules * this.rules.length + margin * 2)
        }

        push()
        translate(cellSizeForRules * 1.5, 0)

        for (const rule of this.rules) {
            rule.draw(activeRule)
        }

        pop()

        translate(0, cellSizeForRules * this.rules.length * 1.5)
    }
}
