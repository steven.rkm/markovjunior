let FPS = 60

let cellSize = 15
const spacing = 20

// TODO remove this from global values
let gridWidth = undefined
let gridHeight = undefined

let grid = undefined
let img = undefined

let COLOR_MAPPING = {}

const DEFAULT_SETTINGS = {
    gridWidth: 400,
    gridHeight: 300,
    isStepByStep: false,
    updatesPerInterval: 1,
    intervalTime: 0,
}

let interval = undefined

// let activeRuleSet = ruleSets.find(r => r.name === 'Backtrack Maze')
let activeRuleSet = undefined
let appliedRule = null

let buttonNextRuleset = [spacing, 0, 150, 20 * 1.5]

function setup() {
    // SETUP

    createCanvas(windowWidth, windowHeight)
    frameRate(FPS)

    colorMode(HSB, 100)
    noSmooth()

    angleMode(DEGREES)

    registerSets()

    // set rules and grid and everything else
    reset('River')
}

function registerSets() {
    TILESET_REGISTRY.register(tilesRiver)
    TILESET_REGISTRY.register(tilesSokoban)
    TILESET_REGISTRY.register(tilesBacktrackMaze)
    TILESET_REGISTRY.register(tilesGeneral)

    RULESET_REGISTRY.register(ruleSetRiver)
    RULESET_REGISTRY.register(ruleSetSokoban)
    RULESET_REGISTRY.register(ruleSetBacktrackMaze)
    RULESET_REGISTRY.register(ruleSetTestNeighbours)

    console.log('-- register everything')
    console.log('Tilesets:', TILESET_REGISTRY.list().join(', '))
    console.log('Rulesets:', RULESET_REGISTRY.list().join(', '))
}

function setupColorMapping(tileSet) {
    COLOR_MAPPING = {}

    for (let i = 0; i < tileSet.tiles.length; i++) {
        const tile = tileSet.tiles[i]
        COLOR_MAPPING[tile.value] = color(tile.h, tile.s, tile.l)
    }

    // TODO do not paint EMPTY? it is the edge afterall
    COLOR_MAPPING[EMPTY] = color(0, 0, 30)
}

function reset(ruleSetName = undefined) {
    console.log('-- reset')

    if (ruleSetName) {
        try {
            activeRuleSet = RULESET_REGISTRY.get(ruleSetName)
        } catch (e) {
            throw new Error(`ruleSet "${ruleSetName}" could not be found in registry`)
        }
    }

    console.log(activeRuleSet)

    console.log('-- reset -- settings')

    // settings
    const settings = {
        ...DEFAULT_SETTINGS,
        ...activeRuleSet.settings,
    }

    console.log(settings)

    gridWidth = settings.gridWidth
    gridHeight = settings.gridHeight
    isStepByStep = settings.isStepByStep
    updatesPerInterval = settings.updatesPerInterval
    intervalTime = settings.intervalTime

    // COLOR MAPPING

    console.log('-- reset -- setupColorMapping')

    let tileSet = undefined

    try {
        tileSet = TILESET_REGISTRY.get(activeRuleSet.tileSetName)
    } catch (e) {
        throw new Error(`tileSet "${activeRuleSet.tileSetName}" could not be found in registry`)
    }

    setupColorMapping(tileSet)

    console.log(tileSet)
    console.log(COLOR_MAPPING)

    // setup grid and rules

    console.log('-- reset -- grid')
    console.log(`Grid: ${gridWidth} x ${gridHeight}`)

    const backgroundValue = tileSet.getByName(activeRuleSet.backgroundTileName).value
    grid = new Grid(gridWidth, gridHeight, backgroundValue)
    grid.makeEdge(EMPTY)

    // setup grid customizations
    const fixedTiles = activeRuleSet.fixedTiles
    for (const [x, y, tileName] of fixedTiles) {
        const tileValue = tileSet.getByName(tileName).value
        grid.set(x, y, tileValue)
    }

    console.log('-- reset -- buffer image')

    // create buffer image
    img = createImage(gridWidth, gridHeight)

    // set image
    img.loadPixels()
    for (let i = 0; i < grid.rows; i++) {
        for (let j = 0; j < grid.cols; j++) {
            updateCellWithAlpha(i, j, grid.get(i, j))
        }
    }
    img.updatePixels()

    console.log('-- reset -- rules init')

    // init rules
    activeRuleSet.init(gridWidth, gridHeight, tileSet)

    // START INTERVAL

    console.log('-- reset -- interval')

    // TODO work with isHalted
    appliedRule = activeRuleSet.rules[0]

    // reset counters
    countUpdates = 0
    countGetCell = 0

    // TODO move interval out of setup

    // setup interval
    interval = new IntervalHandler(() => {
        // console.log('interval')

        for (let i = 0; i < updatesPerInterval; i++) {
            if (!interval.isPaused() && appliedRule !== null) {
                update()

                if (isStepByStep) {
                    interval.pause()
                }
            }
        }
    }, intervalTime)

    performance.mark('start')

    if (!isStepByStep) interval.start()
}

function mouseClicked() {
    // reset()

    // if(isInsideRect(mouseX, mouseY, buttonNextRuleset)) {
    //     const index = ruleSets.indexOf(activeRuleSet)
    //     setupRuleSet(ruleSets[(index+1)%ruleSets.length])
    //     return
    // }

    if (appliedRule === null) {
        reset(activeRuleSet.name)
    } else {
        if (interval.isPaused()) {
            interval.start()
        } else {
            interval.pause()
        }
    }
}

// function doubleClicked() {
//     // if(isInsideRect(mouseX, mouseY, buttonNextRuleset)) {
//     //     return
//     // }
//
//     reset()
// }

// const pressed = false
// function keyPressed() {
//     console.log(keyCode)
//     if (keyCode === 32) {
//         pressed = true
//     }
// }

// window.addEventListener('resize', function(event) {
//     setup()
// }, true);

let portrait = window.matchMedia('(orientation: portrait)')

// portrait.addEventListener("change", function(e) {
//     setup()
// })

// ======== UPDATE
let countUpdates = 0

function update() {
    countUpdates++

    // TODO move to something else like RootRule or something

    // console.log("------------ UPDATE")

    // check for rule matches
    for (const rule of activeRuleSet.rules) {
        if (rule.isAlive && rule.isApplicable(grid)) {
            const [_appliedRule, coords] = rule.apply(grid)

            // TODO pause after each rule -> split rules should count as one block
            // if (appliedRule !== _appliedRule) interval.pause()

            appliedRule = _appliedRule

            const coordinatesWithNeighbors = getCoordinatesWithNeighbors(coords)

            for (const ruleToUpdate of activeRuleSet.rules) {
                if (ruleToUpdate.isAlive) {
                    ruleToUpdate.updateOptions(grid, coordinatesWithNeighbors)
                }
            }

            return // skip other rules!
        }
    }

    performance.mark('end')
    performance.measure('My Function', 'start', 'end')

    const measure = performance.getEntriesByName('My Function')[0]
    console.log(`-- completed`)
    console.log(
        `Execution time: ${Math.floor(measure.duration) / 1000} seconds for ${countUpdates} updates ==> ${Math.floor(countUpdates / (measure.duration / 1000)).toLocaleString('de-DE')} updates/second`,
    )
    console.log(
        `GetCell: ${countGetCell.toLocaleString('de-DE')} ==> ${Math.floor(countGetCell / (measure.duration / 1000)).toLocaleString('de-DE')} calls/second`,
    )

    performance.clearMarks('start')
    performance.clearMarks('end')
    performance.clearMeasures('My Function')

    appliedRule = null
    interval.pause()
}

function getCoordinatesWithNeighbors(coordinates) {
    const result = new Set()

    function addPosition(x, y) {
        result.add(`${x},${y}`)
    }

    coordinates.forEach(([x, y]) => {
        addPosition(x, y)
        addPosition(x - 1, y) // top neighbor
        addPosition(x + 1, y) // bottom neighbor
        addPosition(x, y - 1) // left neighbor
        addPosition(x, y + 1) // right neighbor
    })

    return Array.from(result).map((pos) => pos.split(',').map(Number))
}

// ======== DRAW

function draw() {
    background(0, 0, 20)

    push()
    // translate(width - (gridWidth * cellSize) - spacing, height - (gridHeight * cellSize) - spacing)
    translate(200, spacing)
    drawGrid()
    pop()

    push()
    translate(spacing, spacing)
    drawText()
    pop()

    push()
    // console.log(drawingContext.getTransform()['e'])
    translate(spacing, spacing + 80)
    drawRules()
    pop()

    // TODO fix button
    // push()
    // translate(buttonNextRuleset[0], buttonNextRuleset[1])
    // drawButton()
    // pop()
}

function drawGrid() {
    img.updatePixels()
    const scaling = 15
    image(img, 0, 0, width - spacing * 2 - 200, height - spacing * 2, 0, 0, img.width, img.height, CONTAIN)
}

function drawTile(cell, size = undefined) {
    // gridlines
    strokeWeight(1)
    stroke(0, 0, 20)
    noStroke()

    // set color according to cell
    fill(COLOR_MAPPING[cell])
    rect(0, 0, size ?? cellSize, size ?? cellSize)
}

function updateCell(x, y, c) {
    const color = COLOR_MAPPING[c]

    const index = (y * gridWidth + x) * 4

    // Red.
    img.pixels[index] = red(color) //color.levels[0];
    // Green.
    img.pixels[index + 1] = green(color) //color.levels[1];
    // Blue.
    img.pixels[index + 2] = blue(color) //color.levels[3];
    // Alpha.
    // img.pixels[index + 3] = 255;
}

function updateCellWithAlpha(x, y, c) {
    const color = COLOR_MAPPING[c]

    const index = (y * gridWidth + x) * 4

    // Red.
    img.pixels[index] = red(color) //color.levels[0];
    // Green.
    img.pixels[index + 1] = green(color) //color.levels[1];
    // Blue.
    img.pixels[index + 2] = blue(color) //color.levels[3];
    // Alpha.
    img.pixels[index + 3] = 255
}

function drawRules() {
    // GRID

    // console.log(drawingContext.getTransform()['e'])

    push()

    for (const rule of activeRuleSet.rules) {
        rule.draw(appliedRule)
    }

    // console.log(drawingContext.getTransform())

    pop()
}

function drawText() {
    textAlign(LEFT)
    const size = 25

    // TEXT
    noStroke()
    fill(100, 0, 70)

    textSize(size)
    text(activeRuleSet.name, 0, size)

    if (appliedRule === null) {
        fill(50, 50, 70)
        text('Finished', 0, size * 2.5)
    } else if (interval.isPaused()) {
        fill(0, 50, 70)

        if (appliedRule.description) {
            text(`Paused: ${appliedRule.description}`, 0, size * 2.5)
        } else {
            text(`Paused`, 0, size * 2.5)
        }
    } else if (appliedRule !== null) {
        fill(20, 50, 70)
        if (appliedRule.description) {
            text(`Running: ${appliedRule.description}`, 0, size * 2.5)
        } else {
            text(`Running`, 0, size * 2.5)
        }
    }
}

function drawButton() {
    textAlign(LEFT)
    const size = 25

    textSize(size)

    // TEXT
    stroke(0, 0, 60)
    fill(60, 50, 30)
    rect(0, 0, buttonNextRuleset[2], buttonNextRuleset[3])
    noStroke()
    fill(100, 50, 70)
    text('Next ruleset', 7, size - 1)
}

// ======= UTILS

let countGetCell = 0
function getCell(grid, i, j) {
    countGetCell++
    return grid.get(i, j)
}

function keepInRange(value, start, end) {
    let range = end - start

    if (value < start) return value + range
    if (value > end) return value - range
    return value
}

function getRandomValueFromArray(arr) {
    // Get a random index from the array
    const randomIndex = Math.floor(Math.random() * arr.length)

    // Return the value at the random index
    return arr[randomIndex]
}

function isInsideRect(x, y, rectangle) {
    return (
        x >= rectangle[0] && y >= rectangle[1] && x <= rectangle[0] + rectangle[2] && y <= rectangle[1] + rectangle[3]
    )
}
