const ruleSetSokoban = new RuleSet({
    name: 'Sokoban',
    tileSetName: 'Sokoban',
    backgroundTileName: 'NOTHING',
    rules: [
        new ReplaceRule(['NOTHING'], ['AGENT'], 10),
        new ReplaceRule(['NOTHING'], ['BOX'], 1000),
        new SplitRule([
            new ReplaceRule(['AGENT', 'NOTHING'], ['NOTHING', 'AGENT']),
            new ReplaceRule(['AGENT', 'BOX', 'NOTHING'], ['NOTHING', 'AGENT', 'BOX']),
        ]),
    ],
    settings: {
        gridWidth: 100,
        gridHeight: 100,

        intervalTime: 10,
    },
})
