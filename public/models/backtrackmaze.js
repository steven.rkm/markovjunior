const ruleSetBacktrackMaze = new RuleSet({
    name: 'Backtrack Maze',
    tileSetName: 'Backtrack Maze',
    backgroundTileName: 'NOTHING',
    rules: [
        new ReplaceRule(['NOTHING'], ['CURSOR'], 1),
        new ReplaceRule(['CURSOR', 'NOTHING', 'NOTHING'], ['PATH', 'PATH', 'CURSOR']),
        new ReplaceRule(['CURSOR', 'PATH', 'PATH'], ['BACKTRACK', 'BACKTRACK', 'CURSOR']),
        // TODO breaks because neighbours only go 1 further, not 2 => based on size of rules
    ],
    settings: {
        gridWidth: 400,
        gridHeight: 300,

        updatesPerInterval: 10,
    },
})
