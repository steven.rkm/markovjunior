const ruleSetTestNeighbours = new RuleSet({
    name: 'Test Neighbours',
    tileSetName: 'General',
    backgroundTileName: 'BLACK',
    rules: [new ReplaceRule(['BLACK'], ['RED'], 1), new ReplaceRule(['BLACK', 'RED'], ['RED', 'RED'])],
    settings: {
        gridWidth: 3,
        gridHeight: 10,

        intervalTime: 10,

        isStepByStep: true,
    },
    fixedTiles: [[1, 3, 'RED']],
})
