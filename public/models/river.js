const ruleSetRiver = new RuleSet({
    name: 'River',
    tileSetName: 'River',
    backgroundTileName: 'NOTHING',
    rules: [
        new ReplaceRule(['NOTHING'], ['RED'], { limit: 3, description: 'Seed random red tiles' }),
        new ReplaceRule(['NOTHING'], ['WHITE'], { limit: 3, description: 'Seed random white tiles' }),
        new SplitRule([
            new ReplaceRule(['RED', 'NOTHING'], ['RED', 'RED'], { description: 'Grow red tiles' }),
            new ReplaceRule(['WHITE', 'NOTHING'], ['WHITE', 'WHITE'], { description: 'Grow white tiles' }),
        ]),
        new ReplaceAllRule(['RED', 'WHITE'], ['WATER', 'WATER'], { limit: 1, description: 'Trace edges with water' }),
        new ReplaceAllRule(['RED'], ['NOTHING'], { limit: 1, description: 'Remove red tiles' }),
        new ReplaceAllRule(['WHITE'], ['NOTHING'], { limit: 1, description: 'Remove white tiles' }),
        new ReplaceAllRule(['NOTHING', 'WATER'], ['WATER', 'WATER'], { limit: 1, description: 'Widen river' }),
        new ReplaceAllRule(['NOTHING', 'WATER'], ['GRASS', 'WATER'], {
            limit: 1,
            description: 'Grass next to the river',
        }),
        new ReplaceAllRule(['NOTHING', 'GRASS'], ['GRASS', 'GRASS'], { limit: 2, description: 'Widen grass edge' }),
        new ReplaceRule(['NOTHING'], ['FOREST'], { limit: 5, description: 'Seed random forest tiles' }),
        new ReplaceRule(['NOTHING'], ['GRASS'], { limit: 5, description: 'Seed random grass tiles' }),
        new SplitRule([
            new ReplaceRule(['FOREST', 'NOTHING'], ['FOREST', 'FOREST'], { description: 'Grow forest tiles' }),
            new ReplaceRule(['GRASS', 'NOTHING'], ['GRASS', 'GRASS'], { description: 'Grow grass tiles' }),
        ]),
    ],
    settings: {
        gridWidth: 400,
        gridHeight: 300,

        updatesPerInterval: 2000,
    },
})
